import com.missionessential.resumeparser.ResumeParser;
import gate.Gate;
import org.junit.Test;

import java.io.File;
import java.net.URI;
import java.util.Map;


public class TestParser {

    @Test
    public void testParser() throws Exception {
        URI url = getClass().getResource("/GATEFiles").toURI();
        File gateHome = new File(url);
        Gate.setGateHome(gateHome);

        ResumeParser parser = new ResumeParser();
        File file = new File(getClass().getClassLoader().getResource("CV_BIGBIE.PDF").getFile());
        Map<String, Object> data = parser.parseResume(file);
        System.out.println(data.toString());
    }
}
