package com.missionessential.resumeparser;

import gate.*;
import gate.util.GateException;
import gate.util.persistence.PersistenceManager;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.ToXMLContentHandler;
import org.xml.sax.ContentHandler;

import java.io.*;
import java.net.URL;
import java.util.*;

import static gate.Utils.stringFor;

public class ResumeParser {
    static CorpusController annieController;

    public void init() throws Exception {
        //My GATE resources are in the "/gate" folder of the JAR
        Gate.setPluginsHome(new File(Gate.getGateHome(), "plugins"));
        Gate.setSiteConfigFile(new File(Gate.getGateHome(), "gate.xml"));
        Gate.init();
        Gate.getCreoleRegister().registerDirectories(new File(Gate.getPluginsHome(), "ANNIE").toURI().toURL());
        // initialise ANNIE (this may take several minutes)
        File annieGapp = new File(Gate.getGateHome(), "ANNIEResumeParser.gapp");
        annieController = (CorpusController) PersistenceManager.loadObjectFromFile(annieGapp);
    }

    private File parseToHTMLUsingApacheTikka(File file) throws Exception {
        // determine extension
        String fileName = file.getName();
        String ext = FilenameUtils.getExtension(fileName);
        String outputFileFormat = "";
        // ContentHandler handler;
        if (ext.equalsIgnoreCase("html") | ext.equalsIgnoreCase("pdf") | ext.equalsIgnoreCase("doc") | ext.equalsIgnoreCase("docx")) {
            outputFileFormat = ".html";
        } else if (ext.equalsIgnoreCase("txt") | ext.equalsIgnoreCase("rtf")) {
            outputFileFormat = ".txt";
        } else {
            throw new Exception("Input format of the file " + file + " is not supported.");
        }
        File outputFile = File.createTempFile(FilenameUtils.removeExtension(fileName), outputFileFormat);
        ContentHandler handler = new ToXMLContentHandler();

        InputStream stream = new FileInputStream(file);
        AutoDetectParser parser = new AutoDetectParser();
        Metadata metadata = new Metadata();
        try {
            parser.parse(stream, handler, metadata);
            FileWriter htmlFileWriter = new FileWriter(outputFile);
            htmlFileWriter.write(handler.toString());
            htmlFileWriter.flush();
            htmlFileWriter.close();
            return outputFile;
        } finally {
            stream.close();
        }
    }

    private Map<String, Object> loadGateAndAnnie(File file) throws GateException, IOException {
        Map<String, Object> map = new HashMap<>();
        // create a GATE corpus and add a document for each command-line argument
        Corpus corpus = Factory.newCorpus("Annie corpus");
        URL u = file.toURI().toURL();
        FeatureMap params = Factory.newFeatureMap();
        params.put("sourceUrl", u);
        params.put("preserveOriginalContent", Boolean.TRUE);
        params.put("collectRepositioningInfo", Boolean.TRUE);
        Document resume = (Document) Factory.createResource("gate.corpora.DocumentImpl", params);
        corpus.add(resume);

        // tell the pipeline about the corpus and run it
        annieController.setCorpus(corpus);
        annieController.execute();

        Iterator iter = corpus.iterator();
        if (iter.hasNext()) {
            // should technically be while but I am just dealing with one document
            Document doc = (Document) iter.next();
            AnnotationSet defaultAnnotSet = doc.getAnnotations();

            AnnotationSet curAnnSet;
            Iterator it;
            Annotation currAnnot;

            // Name
            curAnnSet = defaultAnnotSet.get("NameFinder");
            if (curAnnSet.iterator().hasNext()) {
                // only one name will be found.
                currAnnot = curAnnSet.iterator().next();
                String gender = (String) currAnnot.getFeatures().get("gender");
                if (gender != null && gender.length() > 0) {
                    map.put("gender", gender);
                }

                // Needed name Features
                String[] nameFeatures = new String[]{"firstName", "middleName", "surname"};
                for (String feature : nameFeatures) {
                    String s = (String) currAnnot.getFeatures().get(feature);
                    if (s != null && s.length() > 0) {
                        map.put(feature, s);
                    }
                }
            }

            curAnnSet = defaultAnnotSet.get("TitleFinder");
            if (curAnnSet.iterator().hasNext()) {
                // only one title will be found.
                currAnnot = curAnnSet.iterator().next();
                String title = stringFor(doc, currAnnot);
                if (title != null && title.length() > 0) {
                    map.put("title", title);
                }
            }

            String[] annSections = new String[]{"EmailFinder", "AddressFinder", "PhoneFinder", "URLFinder"};
            String[] annKeys = new String[]{"email", "address", "phone", "url"};
            for (short i = 0; i < annSections.length; i++) {
                String annSection = annSections[i];
                curAnnSet = defaultAnnotSet.get(annSection);
                it = curAnnSet.iterator();
                List<String> sectionArray = new ArrayList<>();
                while (it.hasNext()) {
                    // extract all values for each address,email,phone etc..
                    currAnnot = (Annotation) it.next();
                    String s = stringFor(doc, currAnnot);
                    if (s != null && s.length() > 0) {
                        sectionArray.add(s);
                    }
                }
                if (sectionArray.size() > 0) {
                    map.put(annKeys[i], sectionArray);
                }
            }
        }
        return map;
    }

    public Map<String, Object> parseResume(File resume) throws Exception {
        if (!Gate.isInitialised()) {
            init();
        }
        File tikkaConvertedFile = null;
        try {
            tikkaConvertedFile = parseToHTMLUsingApacheTikka(resume);
            if (tikkaConvertedFile != null) {
                return loadGateAndAnnie(tikkaConvertedFile);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (tikkaConvertedFile != null) {
                tikkaConvertedFile.deleteOnExit();
            }
        }
        return null;
    }
}